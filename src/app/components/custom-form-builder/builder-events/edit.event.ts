import { Injectable, Inject, APP_ID } from '@angular/core';
import { ArtapiService } from '../../../artapi.service';
import { editForm } from 'src/app/formio-edit-form/empty/edit-form.index';
import { onEdit } from './onEdit.event';
import { Components } from 'formiojs';
import applySettings  from 'src/app/applySettings';

import * as _Components from "formiojs/components/Components";
import Webform from 'formiojs/Webform'
import * as _builder from "formiojs/utils/builder";
import * as _utils from "formiojs/utils/utils";

import _lodash from 'lodash';

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }
function _objectSpread(...target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source), false).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }


let API;
function getArtApi(api) {
  API = api;
}

function editComp (component, parent, isNew, isJsonEdit, original) {

      let _this10 = this;

      if (!component.key) {
        return;
      }

      let saved = false;
      let componentCopy = _utils.fastCloneDeep(component);
      let ComponentClass = Components.components[componentCopy.type];
      let isCustom = ComponentClass === undefined;
      isJsonEdit = isJsonEdit || isCustom;
      ComponentClass = isCustom ? Components.components.unknown : ComponentClass; // Make sure we only have one dialog open at a time.

      if (this.dialog) {
        this.dialog.close();
        this.highlightInvalidComponents();
      } // This is the render step.
      
      let editFormOptions = _lodash.clone(_lodash.get(this, 'options.editForm', {}));
      if (this.editForm) {
        this.editForm.destroy();
      } // Allow editForm overrides per component.


      let overrides = _lodash.get(this.options, "editForm.".concat(componentCopy.type), {}); // Pass along the form being edited.
      editFormOptions.editForm = this.form;
      editFormOptions.editComponent = component;
      this.editForm = new Webform(_objectSpread({}, _lodash.omit(this.options, ['hooks', 'builder', 'events', 'attachMode', 'skipInit']), {
        language: this.options.language
      }, editFormOptions));
      this.editForm.form = isJsonEdit && !isCustom ? {
        components: [{
          type: 'textarea',
          as: 'json',
          editor: 'ace',
          weight: 10,
          input: true,
          key: 'componentJson',
          label: 'Component JSON',
          tooltip: 'Edit the JSON for this component.'
        }]
      } : ComponentClass.editForm(_lodash.cloneDeep(overrides));
      
      let instance = new ComponentClass(componentCopy);
      this.editForm.submission = isJsonEdit ? {
        data: {
          componentJson: instance.component
        }
      } : {
        data: instance.component
      };

      if (this.preview) {
        this.preview.destroy();
      }

      if (!ComponentClass.builderInfo.hasOwnProperty('preview') || ComponentClass.builderInfo.preview) {
        this.preview = new Webform(_lodash.omit(_objectSpread({}, this.options, {
          preview: true
        }), ['hooks', 'builder', 'events', 'attachMode', 'calculateValue']));
      }

      this.componentEdit = this.ce('div', {
        'class': 'component-edit-container'
      });
      this.setContent(this.componentEdit, this.renderTemplate('builderEditForm', {
        componentInfo: ComponentClass.builderInfo,
        editForm: this.editForm.render(),
        preview: this.preview ? this.preview.render() : false
      }));
      this.dialog = this.createModal(this.componentEdit, _lodash.get(this.options, 'dialogAttr', {})); // This is the attach step.

      this.editForm.attach(this.componentEdit.querySelector('[ref="editForm"]'));
      this.updateComponent(componentCopy);
      this.editForm.on('change', function (event) {
        if (event.changed) {
          // See if this is a manually modified key. Treat custom component keys as manually modified
          if (event.changed.component && event.changed.component.key === 'key' || isJsonEdit) {
            componentCopy.keyModified = true;
          }

          if (event.changed.component && ['label', 'title'].includes(event.changed.component.key)) {
            // Ensure this component has a key.
            if (isNew) {
              if (!event.data.keyModified) {
                _this10.editForm.everyComponent(function (component) {
                  if (component.key === 'key' && component.parent.component.key === 'tabs') {
                    component.setValue(_lodash.camelCase(event.data.title || event.data.label || event.data.placeholder || event.data.type));
                    return false;
                  }
                });
              }

              if (_this10.form) {
                // Set a unique key for this component.
                _builder.uniquify(_this10.findNamespaceRoot(parent.formioComponent.component), event.data);
              }
            }
          } // Update the component.

          _this10.updateComponent(event.data.componentJson || event.data, event.changed);

        }
      });
      this.addEventListener(this.componentEdit.querySelector('[ref="cancelButton"]'), 'click', function (event) {
        event.preventDefault();

        _this10.editForm.detach();

        _this10.emit('cancelComponent', component);

        _this10.dialog.close();

        _this10.highlightInvalidComponents();
      });
      this.addEventListener(this.componentEdit.querySelector('[ref="removeButton"]'), 'click', function (event) {
        event.preventDefault(); // Since we are already removing the component, don't trigger another remove.

        saved = true;

        _this10.editForm.detach();

        _this10.removeComponent(component, parent, original);

        _this10.dialog.close();

        _this10.highlightInvalidComponents();
      });
      this.addEventListener(this.componentEdit.querySelector('[ref="saveButton"]'), 'click', function (event) {
        event.preventDefault();

        if (!_this10.editForm.checkValidity(_this10.editForm.data, true, _this10.editForm.data)) {
          _this10.editForm.setPristine(false);

          _this10.editForm.showErrors();

          return false;
        }

        saved = true;

        _this10.saveComponent(component, parent, isNew, original);
      });

      // trigger, when editForm changed

      let dialogClose = function dialogClose() {
        _this10.editForm.destroy();

        if (_this10.preview) {
          _this10.preview.destroy();

          _this10.preview = null;
        }

        if (isNew && !saved) {
          _this10.removeComponent(component, parent, original);

          _this10.highlightInvalidComponents();
        } // Clean up.


        _this10.removeEventListener(_this10.dialog, 'close', dialogClose);

        _this10.dialog = null;
      };

      this.editForm.triggerChange = (event) => {
        if(!event.fromSubmission) {
          saved = true;
          _this10.saveComponent(component, parent, isNew, original);
        }
      };  

      let comp = component;
      let htmlComp = parent.children[0];

      onEdit.getComponent({htmlComp, comp});

      this.addEventListener(this.dialog, 'close', dialogClose); // Called when we edit a component.
      this.emit('editComponent', component);
}

export const edit = {
  getArtApi,
  editComp
}
 
