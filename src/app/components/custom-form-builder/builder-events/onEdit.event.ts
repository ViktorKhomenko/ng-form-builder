import { editForm } from 'src/app/formio-edit-form/empty/edit-form.index';
import applySettings  from 'src/app/applySettings';
import generalStart from 'src/app/formio-edit-form/pieces/general-start';
import panels from 'src/app/formio-edit-form/pieces/panels';
import { concatMap, map, catchError } from 'rxjs/operators';
import { from, throwError } from 'rxjs';
import _ from 'lodash';
import { Components } from 'angular-formio';

let API;
function getArtApi(api) {
  API = api;
}

let builder;
function getBuilder(builderElement) {
    builder = builderElement;
}

let currentComponent;
function getComponent(element) {
  currentComponent = element;
}

function addFormToComponent(data) {
  document.getElementById('edit-form-loader').classList.remove("none");
  let clearForm = () => obj => {
    if(obj.components.length > 0) {
      if(obj.components[0].type == 'panel') {
        obj.components.forEach(clearForm());
      } 
      else {
        obj.components = [];
      }
    };
  };
  editForm().components[0].components.forEach(clearForm());
  editForm().components[0].components[0].components = [...generalStart];

  let group = data.reduce((r, a) => {
    r[a.NAME] = [...r[a.NAME] || [], a];
    return r;
  }, {});

  from(Object.values(group)).pipe(
    concatMap(groupEl => getValuesForSelect(groupEl))
  ).subscribe(
   (response:any) => {
      if(response.length > 0 ) {
        _.each(editForm().components[0].components, (val, key) => {
           if(val.IDPROPGROUPTYPE && val.IDPROPGROUPTYPE == response[0].IDPROPGROUPTYPE) {
            createFormElement(val);
           } else {
            _.each(val.components, (nestedVal, nestedKey) => {
              if(nestedVal.IDPROPGROUPTYPE && nestedVal.IDPROPGROUPTYPE == response[0].IDPROPGROUPTYPE) {
                createFormElement(nestedVal);
              }
            });
           }
        });
        function createFormElement(formGroup) {
          let settingsProperty;
          let helpText = (response[0].HELPTEXT ? response[0].HELPTEXT : 'NO HELP TEXT').substring(0, 100);
          settingsProperty = {
            weight: 0,
            type: 'textfield',
            input: true,
            key: response[0].VARCODE,
            label: response[0].NAME,
            placeholder: response[0].NAME,
            tooltip: helpText,
            validate: {
              required: true,
            },
          };
          if(response.values && response.values.length > 0) {
            settingsProperty.type = 'select';
            settingsProperty['dataSrc'] = 'values';
            settingsProperty['data'] = {};
            settingsProperty['data']['values'] = [...response.values];
          }
          formGroup.components.push(settingsProperty);
        }
      }
    }, 
    error => console.error(error), 
    () => setNewForm()
  );

  function getValuesForSelect(groupEl) {
      return API.sqlService('artfront_varcontrolprop_dict',{p_name: groupEl[0]['DICTNAME']}).pipe(
          map((result:any) => {
            if(result.length > 0 && groupEl.length == 1) {
              let resultValues = [];
              _.each(result, (val, key) => {
                let label = val.STRINGVAL + ' (' + val.DESCR + ')';
                resultValues.push({label: label, value:val.STRINGVAL});
              });
              groupEl['values'] = resultValues;
            } else {
              groupEl['values'] = [];
            }
            return groupEl;
          }), catchError( error => {
            return throwError( 'Something went wrong!' );
          })
      )
  }

  function checkIfPanelIsFilled() {
      editForm().components[0].components[2].components = 
      editForm().components[0].components[2].components.
      filter((component) => component.components.length > 0);
      editForm().components[0].components[3].components = 
      editForm().components[0].components[3].components.
      filter((component) => component.components.length > 0);
  }

  function setNewForm() {
    checkIfPanelIsFilled();
    builder.editForm.setForm(editForm());
    console.log('DONE');
    document.getElementById('edit-form-loader').classList.add("none");
    applySettings(currentComponent, true);
  }

}

function onEditComponent (component, parent, isNew, isJsonEdit, original) {
    document.getElementById('comp-title').innerHTML = component.type;
    document.getElementById('comp-varname').innerHTML = component.varname;
    // if(!component.idtemplatevar) {
    //     API.sqlService('fb_get_varcontrolprop_newelem',{v_idtemplatevar:component.idvarcontrol, v_idtemplatetype: 1}).subscribe((data) => {
    //       if(data[0].outBinds) {
    //         component['artfrontproperties'] = data[0].outBinds.v_varcontrolprop_tt;
    //         addFormToComponent(data[0].outBinds.v_varcontrolprop_tt);
    //       }
    //     }, 
    //       err => console.log(err)
    //     );
    // } else {
    //   API.sqlService('fb_get_templatevarprop',{v_idtemplatevar:component.idtemplatevar}).subscribe((data) => {
    //     component['artfrontproperties'] = data;
    //     addFormToComponent(data);
    //   }, 
    //     err => console.log(err)
    //   );
    // }
    applySettings(currentComponent, true);
}

export const onEdit = {
    getArtApi,
    getBuilder,
    getComponent,
    onEditComponent
}