import * as _builder from 'formiojs/utils/builder';
import * as _dragula from 'dragula/dist/dragula';
import * as _lodash from 'lodash';
import _nativePromiseOnly from "native-promise-only";
import FormioUtils from 'formiojs/utils';

export default function (element, target, source, sibling) {
    var _this6 = this;
          if (!target) {
            return;
          } // If you try to drop within itself.


          if (element.contains(target)) {
            return;
          }

          var key = element.getAttribute('data-key');
          var type = element.getAttribute('data-type');
          var group = element.getAttribute('data-group');
          var info, isNew, path, index;
          if (key) {
            // This is a new component.
            info = this.getComponentInfo(key, group);

            if (!info && type) {
              info = this.getComponentInfo(type, group);
            }

            isNew = true;
          } else if (source.formioContainer) {
            index = _lodash.default.findIndex(source.formioContainer, {
              key: element.formioComponent.component.key
            });

            if (index !== -1) {
              // Grab and remove the component from the source container.
              info = source.formioContainer.splice(_lodash.default.findIndex(source.formioContainer, {
                key: element.formioComponent.component.key
              }), 1); // Since splice returns an array of one object, we need to destructure it.
              info = info[0];
            }
          } // If we haven't found the component, stop.


          if (!info) {
            return;
          }

          if (target !== source) {
            // Ensure the key remains unique in its new container.
            _builder.default.uniquify(this.findNamespaceRoot(target.formioComponent.component), info);
          }

          var parent = target.formioComponent; // Insert in the new container.
      
          if (target.formioContainer) {
            if (sibling) {
              if (!sibling.getAttribute('data-noattach')) {
                index = _lodash.default.findIndex(target.formioContainer, {
                  key: _lodash.default.get(sibling, 'formioComponent.component.key')
                });
                index = index === -1 ? 0 : index;
              } else {
                index = sibling.getAttribute('data-position');
              }
              if (index !== -1) {
                // info.varname = info.key.toUpperCase();
                let components = [];
                FormioUtils.eachComponent(this.webform.components,  (component) => {
                  if(component.type == info.type) {
                    components.push(+/\d+/.exec(component.key));
                  }
                }, true);
                let maxKey = components.length > 0 ? Math.max(...components) : 0;
                info.key.replace(/[^a-z]/g, '');
                info.key = info.key + (maxKey + 1);
                info.varname = info.key.toUpperCase();
                if(target.formioComponent.constructor.name == 'Webform' ||
                   target.formioComponent.component.key == 'FIELDSET_MAIN' ||
                   target.formioComponent.component.key == 'ROW1FORM' && !info.components) {
                  let uniqueRowKey = '_' + Math.random().toString(36).substr(2, 9);
                  let rowInfo = {
                    "key": `ROW${uniqueRowKey}`,
                    "type": "row-container",
                    "varcontrol": 'row',
                    "varname": `ROW${uniqueRowKey}`,
                    "components": [info]
                  };
                  target.formioContainer.splice(index, 0, rowInfo);
                }
                else {
                  target.formioContainer.splice(index, 0, info);
                }
                if(source.formioContainer && source.formioContainer.length == 0) {
                  let rowIndex = _lodash.default.findIndex((this.getParentElement(this.getParentElement(source))).formioContainer, {
                    key: _lodash.default.get(source, 'formioComponent.component.key')
                  });
                  this.getParentElement(this.getParentElement(source)).formioContainer.splice(rowIndex, 1);
                  this.getParentElement(this.getParentElement(source)).formioComponent.rebuild();
                }
              }

            } else {
              target.formioContainer.push(info);
              if(source.formioContainer && source.formioContainer.length == 0) {
                let rowIndex = _lodash.default.findIndex((this.getParentElement(this.getParentElement(source))).formioContainer, {
                  key: _lodash.default.get(source, 'formioComponent.component.key')
                });
                this.getParentElement(this.getParentElement(source)).formioContainer.splice(rowIndex, 1);
                this.getParentElement(this.getParentElement(source)).formioComponent.rebuild();
              }
            }

            path = this.getComponentsPath(info, parent.component);
            index = _lodash.default.findIndex(_lodash.default.get(parent.schema, path), {
              key: info.key
            });

            if (index === -1) {
              index = 0;
            }
          }

          if (parent && parent.addChildComponent) {
            parent.addChildComponent(info, element, target, source, sibling);
          }
          
          if (isNew && !this.options.noNewEdit) {
            this.editComponent(info, target, isNew);
          } // Only rebuild the parts needing to be rebuilt.


          var rebuild;

          if (target !== source) {
            if (source.formioContainer && source.contains(target)) {
              rebuild = source.formioComponent.rebuild();
            } else if (target.contains(source)) {
              rebuild = target.formioComponent.rebuild();
            } else {
              if (source.formioContainer) {
                rebuild = source.formioComponent.rebuild();
              }

              rebuild = target.formioComponent.rebuild();
            }
          } else {
            // If they are the same, only rebuild one.
            rebuild = target.formioComponent.rebuild();
          }

          if (!rebuild) {
            rebuild = _nativePromiseOnly.default.resolve();
          }
          return rebuild.then(function () {
            _this6.emit('addComponent', info, parent, path, index, isNew);
          });
};