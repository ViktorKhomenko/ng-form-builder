import * as _tooltip from "tooltip.js";
import applySettings  from 'src/app/applySettings';

let builder;
function getBuilder(builderElement) {
    builder = builderElement;
}

function attachComp (element, component) {
    // Add component to element for later reference.
    element.formioComponent = component;
    component.loadRefs(element, {
      removeComponent: 'single',
      editComponent: 'single',
      moveComponent: 'single',
      copyComponent: 'single',
      pasteComponent: 'single',
      editJson: 'single'
    });

    if (component.refs.copyComponent) {
      new _tooltip.default(component.refs.copyComponent, {
        trigger: 'hover',
        placement: 'top',
        title: builder.t('Copy')
      });
      component.addEventListener(component.refs.copyComponent, 'click', function () {
        return builder.copyComponent(component);
      });
    }

    if (component.refs.pasteComponent) {
      var pasteToolTip = new _tooltip.default(component.refs.pasteComponent, {
        trigger: 'hover',
        placement: 'top',
        title: builder.t('Paste below')
      });
      component.addEventListener(component.refs.pasteComponent, 'click', function () {
        pasteToolTip.hide();

        builder.pasteComponent(component);
      });
    }

    if (component.refs.moveComponent) {
      new _tooltip.default(component.refs.moveComponent, {
        trigger: 'hover',
        placement: 'top',
        title: builder.t('Move')
      });
    }

    var parent = builder.getParentElement(element);

    if (component.refs.editComponent) {
      new _tooltip.default(component.refs.editComponent, {
        trigger: 'hover',
        placement: 'top',
        title: builder.t('Edit')
      });
      component.addEventListener(component.refs.editComponent, 'click', function () {
        return builder.editComponent(component.schema, parent, false, false, component.component);
      });
    }

    if (component.refs.editJson) {
      new _tooltip.default(component.refs.editJson, {
        trigger: 'hover',
        placement: 'top',
        title: builder.t('Edit JSON')
      });
      component.addEventListener(component.refs.editJson, 'click', function () {
        return builder.editComponent(component.schema, parent, false, true, component.component);
      });
    }

    if (component.refs.removeComponent) {
      new _tooltip.default(component.refs.removeComponent, {
        trigger: 'hover',
        placement: 'top',
        title: builder.t('Remove')
      });
      component.addEventListener(component.refs.removeComponent, 'click', function () {
        return this.removeComponent(component.schema, parent, component.component);
      });
    }

    applySettings({element, component}, false);
    
    return element;
  };

export const attachComponent = {
    getBuilder,
    attachComp
}