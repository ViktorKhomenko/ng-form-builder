import _nativePromiseOnly from "native-promise-only";
import _lodash from 'lodash';
import FormioUtils from 'formiojs/utils';
import applySettings  from 'src/app/applySettings';

export default function(component, changed) {
    // Update the preview.
    
    if (this.preview) {
        this.preview.form = {
          components: [_lodash.omit(component, ['hidden', 'conditional', 'calculateValue', 'logic', 'autofocus', 'customConditional'])]
        };
        var previewElement = this.componentEdit.querySelector('[ref="previe`w"]');

        if (previewElement) {
          this.setContent(previewElement, this.preview.render());
          this.preview.attach(previewElement);
        }
      } // Change the "default value" field to be reflective of this component.


      var defaultValueComponent = (FormioUtils.getComponent)(this.editForm.components, 'defaultValue');

      if (defaultValueComponent) {
        var defaultChanged = changed && (changed.component && changed.component.key === 'defaultValue' || changed.instance && defaultValueComponent.hasComponent && defaultValueComponent.hasComponent(changed.instance));

        if (!defaultChanged) {
          _lodash.default.assign(defaultValueComponent.component, _lodash.default.omit(component, ['key', 'label', 'placeholder', 'tooltip', 'hidden', 'autofocus', 'validate', 'disabled', 'defaultValue', 'customDefaultValue', 'calculateValue', 'conditional', 'customConditional']));

          var parentComponent = defaultValueComponent.parent;
          var tabIndex = -1;
          var index = -1;
          parentComponent.tabs.some(function (tab, tIndex) {
            tab.some(function (comp, compIndex) {
              if (comp.id === defaultValueComponent.id) {
                tabIndex = tIndex;
                index = compIndex;
                return true;
              }

              return false;
            });
          });

          if (tabIndex !== -1 && index !== -1) {
            var sibling = parentComponent.tabs[tabIndex][index + 1];
            parentComponent.removeComponent(defaultValueComponent);
            var newComp = parentComponent.addComponent(defaultValueComponent.component, defaultValueComponent.data, sibling);

            _lodash.default.pull(newComp.validators, 'required');

            parentComponent.tabs[tabIndex].splice(index, 1, newComp);

            newComp.checkValidity = function () {
              return true;
            };

            newComp.build(defaultValueComponent.element);
          }
        }
      } // Called when we update a component.

      this.emit('updateComponent', component);

};