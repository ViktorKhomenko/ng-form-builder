import _nativePromiseOnly from "native-promise-only";
import applySettings  from 'src/app/applySettings';

export default function(component, parent, isNew, original) {
    var _this9 = this;
    var parentContainer = parent ? parent.formioContainer : this.container;
    var parentComponent = parent ? parent.formioComponent : this;
    var path = parentContainer ? this.getComponentsPath(component, parentComponent.component) : '';
    
    if (!original) {
      original = parent.formioContainer.find(function (comp) {
        return comp.key === component.key;
      });
    }

    var index = parentContainer ? parentContainer.indexOf(original) : 0;
    // if (index !== -1) {
      var submissionData = this.editForm.submission.data;
      submissionData.label = submissionData.varlabel; // update label property as well
      submissionData = submissionData.componentJson || submissionData;
      if (parentContainer) {
        parentContainer[index] = submissionData;
      } else if (parentComponent && parentComponent.saveChildComponent) {
        parentComponent.saveChildComponent(submissionData);
      }

      var rebuild = parentComponent.rebuild() || _nativePromiseOnly.resolve();
      return rebuild.then(function () {
        var schema = parentContainer ? parentContainer[index] : [];
        parentComponent.getComponents().forEach(function (component) {
          if (component.key === schema.key) {
            schema = component.schema;
          }
        });


        document.getElementById('comp-title').innerHTML = schema.type;
        document.getElementById('comp-varname').innerHTML = schema.varname;

        _this9.emit('saveComponent', schema, component, parentComponent.schema, path, index, isNew);        

        _this9.emit('change', _this9.form);
        
        _this9.highlightInvalidComponents();

      });
    // }

    // this.highlightInvalidComponents();
    // return _nativePromiseOnly.resolve();

};