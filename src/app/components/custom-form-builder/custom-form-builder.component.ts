import { Component, OnInit, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import builderOptions from './builder-options';
import createModal from './builder-events/createModal.event';
import applySettings from '../../applySettings';
import onDrop from './builder-events/onDrop.event';
import update from './builder-events/update.event';
import { attachComponent } from './builder-events/attachComponent';
import { ArtapiService } from '../../artapi.service';
import { onEdit } from './builder-events/onEdit.event';
import onSave from './builder-events/onSave.event';
import { FormBuilderComponent } from 'angular-formio';
import * as _tooltip from "tooltip.js";
import FormioUtils from 'formiojs/utils';
import { Components } from 'formiojs';
import { editForm } from 'src/app/formio-edit-form/empty/edit-form.index';
import { edit } from './builder-events/edit.event';
import _lodash from 'lodash';
@Component({
  selector: 'app-custom-form-builder',
  templateUrl: './custom-form-builder.component.html',
  styleUrls: ['./custom-form-builder.component.css']
})
export class CustomFormBuilderComponent implements OnInit, AfterViewInit {
  @ViewChild(FormBuilderComponent) builder;
  @Input() form: any;
  @Output() updateForm = new EventEmitter<any>();
  public formBuilderOptions: any = builderOptions;
  components: Object = {};
  constructor(private artapi: ArtapiService) { }

  ngOnInit(): void {
    
  }

  ngAfterViewInit() {
    onEdit.getArtApi(this.artapi);
    this.builder.ready.then((builder) => {
      onEdit.getBuilder(builder);
      attachComponent.getBuilder(builder);
      document.querySelector('.formbuilder').addEventListener('click', (event) => {
        if((event.target as HTMLElement).closest('.builder-component') != null) {
          ((event.target as HTMLElement).closest('.builder-component').querySelector('.component-settings-button-edit') as HTMLElement).click()
        }
      });
      window.addEventListener('scroll', function() {
        if (Math.round(window.scrollY) > 100){
          document.querySelector('.formcomponents').classList.add('sticky');
      }
      else {
          document.querySelector('.formcomponents').classList.remove('sticky');
        }
      });
      builder.createModal = createModal;
      builder.onDrop = onDrop;
      builder.saveComponent = onSave;
      builder.on('editComponent', onEdit.onEditComponent);
      builder.options.hooks.attachComponent = attachComponent.attachComp;
      builder.editComponent = edit.editComp;
      FormioUtils.eachComponent(builder.webform.components,  (component) => {
        component.loadRefs(component.element, {
          showTitle: 'single',
          removeComponent: 'single',
          editComponent: 'multiple',
          dragComponent: 'single'
        });
        if (component.refs.showTitle) {
          new _tooltip.default(component.refs.showTitle, {
            trigger: 'hover',
            placement: 'top',
            title: builder.t(component.schema.key)
          });
        }
      }, true);
    });   
  }
}
