import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-rendered-form',
  templateUrl: './rendered-form.component.html',
  styleUrls: ['./rendered-form.component.css']
})
export class RenderedFormComponent implements OnInit {
  @Input() form: Object;
  
  constructor() { }

  ngOnInit(): void {
  }

}
