import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { editForm }  from './formio-edit-form/edit-form.index';
import { Components } from 'formiojs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private _route: ActivatedRoute, private _router: Router) {
    // for (let component in Components.components) {
    //   Components.components[component].editForm = editForm;
    // }    
    Components.components['textarea'].editForm = editForm;
    Components.components['select'].editForm = editForm;
    Components.components['radio'].editForm = editForm;
    Components.components['button'].editForm = editForm;
    Components.components['datagrid'].editForm = editForm;
    Components.components['fieldset'].editForm = editForm;

    Components.components['row-container'].editForm = editForm;
    Components.components['label'].editForm = editForm;
    Components.components['treeview'].editForm = editForm;
    Components.components['tab'].editForm = editForm;
    Components.components['list-v-container'].editForm = editForm;
    Components.components['list-h-container'].editForm = editForm;
    Components.components['grid-container'].editForm = editForm;
    Components.components['accordion'].editForm = editForm;
  }
}
