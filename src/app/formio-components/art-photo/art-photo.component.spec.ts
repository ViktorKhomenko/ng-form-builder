import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtPhotoComponent } from './art-photo.component';

describe('ArtPhotoComponent', () => {
  let component: ArtPhotoComponent;
  let fixture: ComponentFixture<ArtPhotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtPhotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtPhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
