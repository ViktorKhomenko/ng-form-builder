import { Injector } from '@angular/core';
import { ArtPhotoComponent } from './art-photo.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'artPhoto',
  selector: 'formio-art-photo',
  title: 'art-photo',
  group: 'special',
  icon: 'sticky-note-o',
  editForm: editForm,
  schema: {
    varcontrol: 'art-photo',
    idvarcontrol: 53,
    key: 'ART-PHOTO1',
    varname: 'ART-PHOTO1',
    varbind: '',
    label: 'Art-photo',
    varlabel: 'Art-photo',
  }
};

export function registerArtPhotoComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, ArtPhotoComponent, injector);
}