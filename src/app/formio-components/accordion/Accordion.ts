import { Components } from 'formiojs';
const NestedComponent = (Components as any).components.nested;
import * as _tooltip from "tooltip.js";
import  { editForm }  from './edit-form/edit-form.index';

export default class AccordionComponent extends NestedComponent {
  static schema(...extend) {
    return NestedComponent.schema({
      type: 'accordion',
      varcontrol: 'accordion',
      idvarcontrol: 16,
      key: 'ACCORDION1',
      varname: 'ACCORDION1',
      varlabel: 'Accordion1',
      varbind: '',
      components: []
    }, ...extend);
  }

  static get builderInfo() {
    return {
      title: 'accordion',
      group: 'layout',
      icon: 'bars',
      weight: 70,
      schema: AccordionComponent.schema()
    };
  }

  get defaultSchema() {
    return AccordionComponent.schema();
  }

  get className() {
    return `form-group ${super.className} accordion`;
  }

  get templateName() {
    return 'fieldset';
  }

  attach(element) {
    const refs = {'showTitle': ''};
    this.loadRefs(element, refs);
    this.tooltip = new _tooltip.default(element, {
      trigger: 'hover click',
      placement: 'top',
      html: true,
      title: this.t(this.component.key)
    });
    return super.attach(element);
  }


  constructor(component, options, data) {
    super(component, options, data);
    this.noField = true;
    this.component = component;
  }
}

AccordionComponent.editForm = editForm;