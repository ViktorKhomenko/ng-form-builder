import { Injector } from '@angular/core';
import { SpanComponent } from './span.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'span',
  selector: 'formio-span',
  title: 'span',
  group: 'special',
  icon: 'paragraph',
  editForm: editForm,
  schema: {
    type: 'span',
    varcontrol: 'span',
    idvarcontrol: 17,
    key: 'SPAN1',
    varname: 'SPAN1',
    varbind: '',
  }
};

export function registerSpanComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, SpanComponent, injector);
}