import { Injector } from '@angular/core';
import { ChartComponent } from './chart.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'chart',
  selector: 'formio-chart',
  title: 'chart',
  group: 'special',
  icon: 'sticky-note-o',
  editForm: editForm,
  schema: {
    type: 'chart',
    varcontrol: 'chart',
    idvarcontrol: 43,
    key: 'CHART1',
    varname: 'CHART1',
    varbind: '',
    label: 'Chart',
    varlabel: 'Chart',
  }
};

export function registerChartComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, ChartComponent, injector);
}