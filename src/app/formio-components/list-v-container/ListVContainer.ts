import { Components } from 'formiojs';
const NestedComponent = (Components as any).components.nested;
import * as _tooltip from "tooltip.js";
import  { editForm }  from './edit-form/edit-form.index';

export default class ListVContainerComponent extends NestedComponent {
  static schema(...extend) {
    return NestedComponent.schema({
      type: 'row-container',
      varcontrol: "list-v-container",
      idvarcontrol: 69,
      key: "LISTCONTAINER1",
      varname: "LISTCONTAINER1",
      label: 'ListContainer1',
      varlabel: "ListContainer1",
      varbind: '',
      input: false,
      persistent: false,
      components: []	
    }, ...extend);
  }

  static get builderInfo() {
    return {
      title: 'row-container',
      group: 'layout',
      icon: 'fa fa-tasks',
      weight: 70,
      schema: ListVContainerComponent.schema()
    };
  }

  get defaultSchema() {
    return ListVContainerComponent.schema();
  }

  get className() {
    return `form-group ${super.className} list-v-container`;
  }

  get templateName() {
    return 'fieldset';
  }

  attach(element) {
    const refs = {'showTitle': ''};
    this.loadRefs(element, refs);
    this.tooltip = new _tooltip.default(element, {
      trigger: 'hover click',
      placement: 'top',
      html: true,
      title: this.t(this.component.key)
    });
    return super.attach(element);
  }


  constructor(component, options, data) {
    super(component, options, data);
    this.noField = true;
    this.component = component;
  }
}

ListVContainerComponent.editForm = editForm;