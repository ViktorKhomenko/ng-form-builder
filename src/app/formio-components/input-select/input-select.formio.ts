import { Injector } from '@angular/core';
import { InputSelectComponent } from './input-select.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'input-select',
  selector: 'input-select',
  title: 'select',
  group: 'custom',
  icon: 'th-list',
  editForm: editForm,
  schema: {
    type: 'select',
    varcontrol: 'select',
    idvarcontrol: 1,
    key: 'SELECT1',
    varname: 'SELECT1',
    varbind: '',
    label: 'Select',
    varlabel: 'Select',
    input: true,
    widget: "choicesjs"
  }
};

export function registerInputSelectComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, InputSelectComponent, injector);
}