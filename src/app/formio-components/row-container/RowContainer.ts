import { Components } from 'formiojs';
const NestedComponent = (Components as any).components.nested;
import * as _tooltip from "tooltip.js";

export default class RowComponent extends NestedComponent {
  static schema(...extend) {
    return NestedComponent.schema({
      type: 'row-container',
      label:'row-container',
      varcontrol: 'row',
      idvarcontrol: 31,
      key: 'ROW1',
      varname: 'ROW1',
      input: false,
      persistent: false,
      components: []	
    }, ...extend);
  }

  static get builderInfo() {
    return {
      title: 'row-container',
      group: 'layout',
      icon: 'fa fa-tasks',
      weight: 70,
      schema: RowComponent.schema()
    };
  }

  get defaultSchema() {
    return RowComponent.schema();
  }

  get className() {
    return `form-group ${super.className} row-container`;
  }

  get templateName() {
    return 'fieldset';
  }

  attach(element) {
    const refs = {'showTitle': ''};
    this.loadRefs(element, refs);
    this.tooltip = new _tooltip.default(element, {
      trigger: 'hover click',
      placement: 'top',
      html: true,
      title: this.t(this.component.key)
    });
    return super.attach(element);
  }


  constructor(component, options, data) {
    super(component, options, data);
    this.noField = true;
    this.component = component;
  }
}