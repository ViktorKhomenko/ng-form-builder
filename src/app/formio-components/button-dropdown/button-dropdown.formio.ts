import { Injector } from '@angular/core';
import { ButtonDropdownComponent } from './button-dropdown.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'buttonDropdown',
  selector: 'formio-button-dropdown',
  title: 'button[dropdown]',
  group: 'special',
  icon: 'stop',
  editForm: editForm,
  schema: {
    type: 'button[dropdown]',
    varcontrol: 'button[dropdown]',
    idvarcontrol: 21,
    key: 'BUTTONDD1',
    varname: 'BUTTONDD1',
    varbind: '',
    label: 'Кнопка',
    varlabel: 'Кнопка',
  }
};

export function registerButtonDropdownComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, ButtonDropdownComponent, injector);
}