import { Injector } from '@angular/core';
import { VideoComponent } from './video.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'video',
  selector: 'formio-video',
  title: 'video',
  group: 'special',
  icon: 'sticky-note-o',
  editForm: editForm,
  schema: {
    type: 'video',
    varcontrol: 'video',
    idvarcontrol: 34,
    key: 'VIDEO1',
    varname: 'VIDEO1',
    varbind: '',
    label: 'Video',
    varlabel: 'Video',
  }
};

export function registerVideoComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, VideoComponent, injector);
}