import { Components } from 'formiojs';
const NestedComponent = (Components as any).components.nested;
import * as _tooltip from "tooltip.js";
import  { editForm }  from './edit-form/edit-form.index';

export default class GridContainerComponent extends NestedComponent {
  static schema(...extend) {
    return NestedComponent.schema({
      type: 'grid-container',
      varcontrol: 'grid-container',
      idvarcontrol: 67,
      key: 'GRIDCONTAINER1',
      varname: 'GRIDCONTAINER1',
      varbind: '',
      autofocus: false,
      input: true,
      tree: true,
      tableView: true,
      protected: false,
      persistent: true,
      hidden: false,
      clearOnHide: true,
      components: []	
    }, ...extend);
  }

  static get builderInfo() {
    return {
      title: 'grid-container',
      group: 'layout',
      icon: 'th',
      weight: 70,
      schema: GridContainerComponent.schema()
    };
  }

  get defaultSchema() {
    return GridContainerComponent.schema();
  }

  get className() {
    return `form-group ${super.className} grid-container`;
  }

  get templateName() {
    return 'fieldset';
  }

  attach(element) {
    const refs = {'showTitle': ''};
    this.loadRefs(element, refs);
    this.tooltip = new _tooltip.default(element, {
      trigger: 'hover click',
      placement: 'top',
      html: true,
      title: this.t(this.component.key)
    });
    return super.attach(element);
  }


  constructor(component, options, data) {
    super(component, options, data);
    this.noField = true;
    this.component = component;
  }
}


GridContainerComponent.editForm = editForm;