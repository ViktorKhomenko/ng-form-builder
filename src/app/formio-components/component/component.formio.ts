import { Injector } from '@angular/core';
import { ComponentComponent } from './component.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'component',
  selector: 'formio-component',
  title: 'component',
  group: 'special',
  icon: 'cubes',
  editForm: editForm,
  schema: {
    type: 'component',
    varcontrol: 'component',
    idvarcontrol: 15,
    key: 'COMPONENT1',
    varname: 'COMPONENT1',
    varbind: '',
    label: 'Компонент',
    varlabel: 'Компонент',
    bindings: [],
  }
};

export function registerComponentComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, ComponentComponent, injector);
}