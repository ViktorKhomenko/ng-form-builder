const editFormGeneral = [
    {
      weight: 0,
      type: 'textfield',
      input: true,
      key: 'varname',
      label: 'Название поля',
      placeholder: 'Название поля',
      validate: {
        required: true,
      },
    },
    {
      weight: 10,
      type: 'select',
      input: true,
      key: 'idvarcontrol',
      label: 'Тип поля',
      placeholder: '',
      tooltip: '',
      dataSrc: 'values',
      data: {
            values: [
                {
                    label: 'accordion',
                    value: 'accordion'
                },
                {
                  label: 'alert',
                  value: 'alert'
                }, 
                {
                  label: 'art-photo',
                  value: 'artPhoto'
                }, 
                {
                  label: 'autocomplete',
                  value: 'autoComplete'
                }, 
                {
                  label: 'bpmn-modeler',
                  value: 'bpmnModeler'
                },
                {
                  label: 'bpmn-viewer',
                  value: 'bpmnViewer'
                },
                {
                  label: 'button',
                  value: 'button'
                },
                {
                  label: 'button[dropdown]',
                  value: 'buttonDropdown'
                },
                {
                  label: 'button[file]',
                  value: 'buttonFile'
                },
                {
                  label: 'button[group]',
                  value: 'buttonGroup'
                },
                {
                  label: 'chart',
                  value: 'chart'
                },
                {
                  label: 'chart',
                  value: 'chart'
                },
                {
                  label: 'codeMirror',
                  value: 'code-mirror'
                },
                {
                  label: 'component',
                  value: 'component'
                },
                {
                  label: 'fastfind',
                  value: 'fastfind'
                },
                {
                  label: 'fieldset',
                  value: 'fieldset'
                },
                {
                  label: 'g-maps',
                  value: 'gMaps'
                },
                {
                  label: 'grid',
                  value: 'datagrid'
                },
                {
                  label: 'image',
                  value: 'image'
                },
                {
                  label: 'input[checkbox]',
                  value: 'checkbox'
                },
                {
                  label: 'input[date]',
                  value: 'day'
                },
                {
                  label: 'input[datetime]',
                  value: 'inputDateTime'
                },
                {
                  label: 'input[email]',
                  value: 'email'
                },
                {
                  label: 'input[hidden]',
                  value: 'email'
                },
                {
                  label: 'input[password]',
                  value: 'password'
                },
                {
                  label: 'input[password]',
                  value: 'password'
                },
                {
                  label: 'input[radio]',
                  value: 'radio'
                },
                {
                  label: 'input[text]',
                  value: 'texfield'
                },
                {
                  label: 'label',
                  value: 'label'
                },
                {
                  label: 'list-h-container',
                  value: 'list-h-container'
                },
                {
                  label: 'list-v-container',
                  value: 'list-v-container'
                },
                {
                  label: 'md-progress-linear',
                  value: 'mdProgressLinear'
                },
                {
                  label: 'md-slider',
                  value: 'mdSlider'
                },
                {
                  label: 'qrReader',
                  value: 'qr-reader'
                },
                {
                  label: 'qrReader',
                  value: 'qr-reader'
                },
                {
                  label: 'select',
                  value: 'select'
                },
                {
                  label: 'span',
                  value: 'span'
                },
                {
                  label: 'switch',
                  value: 'switch'
                },
                {
                  label: 'tab',
                  value: 'tab'
                },
                {
                  label: 'template',
                  value: 'template'
                },
                {
                  label: 'textarea',
                  value: 'textarea'
                },
                {
                  label: 'treeview',
                  value: 'treeview'
                },
                {
                  label: 'ui-carousel',
                  value: 'uiCarousel'
                },
                {
                  label: 'video',
                  value: 'video'
                }
            ]
        },
      },
    {
      weight: 20,
      type: 'textfield',
      input: true,
      key: 'varbind',
      label: 'Значение',
      placeholder: '',
    },
    {
      weight: 20,
      type: 'textfield',
      input: true,
      key: 'varlabel',
      label: 'Заголовок',
      placeholder: '',
    },
    {
      weight: 20,
      type: 'select',
      input: true,
      key: 'idartdatatype',
      label: 'Тип данных',
      placeholder: '',
      dataSrc: 'values',
      data: {
        values: [
          {
            label: 'string',
            value: 98
          },
          {
            label: 'boolean',
            value: 99
          }, 
          {
            label: 'decimal',
            value: 100
          }, 
          {
            label: 'dateTime',
            value: 101
          }, 
          {
            label: 'time',
            value: 102
          },
          {
            label: 'date',
            value: 103
          }, 
          {
            label: 'integer',
            value: 104
          }, 
        ]
      },
    },
    {
      weight: 20,
      type: 'textfield',
      input: true,
      key: 'pattern-input-text',
      label: 'pattern',
      placeholder: '',
      tooltip: 'Указывает регулярное выражение, согласно которому требуется вводить и проверять данные в поле формы. Если присутствует атрибут pattern, то форма не будет отправляться, пока поле не будет заполнено правильно',
    },
    {
      weight: 20,
      type: 'textfield',
      input: true,
      key: 'class',
      label: 'class',
      placeholder: '',
      tooltip: 'В данный момент не рекомендуется использовать. Для всех значений ранее прописываемых в данном поле заведено отдельное свойство, расположенное ниже данной строки.',
    },
    {
      weight: 20,
      type: 'select',
      input: true,
      key: 'disabled-input-text',
      label: 'disabled',
      placeholder: '',
      tooltip: 'Свойство, определяющее доступность поля.',
      dataSrc: 'values',
      data: {
        values: [
            {
                label: 'false-value (значение=false)',
                value: '0'
            },
            {
              label: 'true-value (значение=true)',
              value: '1'
            },
        ]
      },
    },
    {
      weight: 20,
      type: 'select',
      input: true,
      key: 'autocomplete-html',
      label: 'autocomplete-html',
      placeholder: '',
      tooltip: 'Автозаполнение Автозаполнение производит браузер, который запоминает написанные при первом вводе значения, а затем подставляет их при повторном наборе в поля формы. Возможные значения: on/off/autofill field names(наименования полей автозаполнения)',
      dataSrc: 'values',
      data: {
        values: [
            {
                label: 'autocomlete-off (Не сохранять отправленные данные и автоматически не заполнять поля)',
                value: 'off'
            },
            {
              label: 'autocomlete-on (Сохранять отправленные данные и автоматически заполнять поля)',
              value: 'on'
            },
        ]
      },
    },
    {
      weight: 20,
      type: 'select',
      input: true,
      key: 'input_inputmode',
      label: 'inputmode',
      placeholder: '',
      tooltip: 'Предоставляет подсказку браузерам для устройств с экранными клавиатурами, чтобы помочь им решить, какую клавиатуру отображать.',
      dataSrc: 'values',
      data: {
        values: [
            {
                label: 'email (email)',
                value: 'email'
            },
            {
              label: 'full-width-latin (full-width-latin)',
              value: 'full-width-latin'
            },
            {
              label: 'full-width-latin (full-width-latin)',
              value: 'full-width-latin'
            },
            {
              label: 'kana (kana)',
              value: 'kana'
            },
            {
              label: 'katakana (katakana)',
              value: 'katakana'
            },
            {
              label: 'latin (latin)',
              value: 'latin'
            },
            {
              label: 'latin-name (latin-name)',
              value: 'latin-name'
            },
            {
              label: 'latin-prose (latin-prose)',
              value: 'latin-prose'
            },
            {
              label: 'numeric (numeric)',
              value: 'numeric'
            },
            {
              label: 'tel (tel)',
              value: 'tel'
            },
            {
              label: 'tel (tel)',
              value: 'tel'
            },
            {
              label: 'url (url)',
              value: 'url'
            },
            {
              label: 'verbatim (verbatim)',
              value: 'verbatim'
            },
        ]
      },
    },
    {
      weight: 20,
      type: 'select',
      input: true,
      key: 'forcetranslate',
      label: 'force-translate',
      placeholder: '',
      tooltip: 'Признак необходимости выставления метки перевода. Применяется к заголовку и к значению поля. Используется, если неизвестно значение на этапе генерации кода страницы, т.е. нельзя определить необходимость перевода. Метка будет добавлена, если поле задано в виде { {$ctrl....} }.',
      dataSrc: 'values',
      data: {
        values: [
            {
                label: 'true (true Для логических (сокращенных) атрибутов с типом использования "использовать введенное значение" - т.к. факт наличия такого атрибута подразумевает значение true.)',
                value: '1'
            }
        ]
      },
    },
    {
      weight: 20,
      type: 'textfield',
      input: true,
      key: 'aria-label',
      label: 'aria-label',
      placeholder: '',
      tooltip: 'Определяет строковое значение, которое обозначает текущий элемент',
    },
    {
      weight: 20,
      type: 'textfield',
      input: true,
      key: 'input-placeholder',
      label: 'input-placeholder',
      placeholder: '',
      tooltip: 'Текст, подставляемый в placeholder. Отображается при фокусировке',
    },
  ];
  
  export default editFormGeneral;