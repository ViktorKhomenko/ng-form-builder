import { Injector } from '@angular/core';
import { InputHiddenComponent } from './input-hidden.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'inputHidden',
  selector: 'input-hidden',
  title: 'input[hidden]',
  group: 'special',
  icon: 'user-secret',
  editForm: editForm,
  schema: {
    type: 'input[hidden]',
    varcontrol: 'input[hidden]',
    idvarcontrol: 6,
    key: 'HIDDEN1',
    varname: 'HIDDEN1',
    varbind: '',
    label: 'Hidden',
    varlabel: 'Hidden',
    input: true,
  }
};

export function registerInputHiddenComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, InputHiddenComponent, injector);
}