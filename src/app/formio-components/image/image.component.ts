import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormioCustomComponent } from 'angular-formio';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements FormioCustomComponent<any> {

  @Input()
  value: string;

  @Output()
  valueChange = new EventEmitter<string>();

  @Input()
  disabled: boolean;

}
