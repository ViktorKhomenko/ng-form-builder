import { Injector } from '@angular/core';
import { MdProgressLinearComponent } from './md-progress-linear.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'mdProgressLinear',
  selector: 'formio-md-progress-linear',
  title: 'md-progress-linear',
  group: 'special',
  icon: 'sticky-note-o',
  editForm: editForm,
  schema: {
    type: 'md-progress-linear',
    varcontrol: 'md-progress-linear',
    idvarcontrol: 56,
    key: 'MD-PROGRESS-LINEAR1',
    varname: 'MD-PROGRESS-LINEAR1',
    varbind: '',
    label: 'Md-progress-linear',
    varlabel: 'Md-progress-linear',
  }
};

export function registerMdProgressLinearComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, MdProgressLinearComponent, injector);
}