const editFormPositioning = [
    {
      weight: 0,
      type: 'select',
      input: true,
      key: 'layout-alignment',
      label: 'layout-alignment',
      placeholder: '',
      tooltip: 'Форматирование для flexbox. Запись в формате layout-align-<Выравнивание по вертикали>-<Выравнивание по горизонтали> для выравнивания в столбец Для выравнивания в строку - layout-align-<Выравнивание по горизонтали>-<Выравнивание по вертикали> Возможные значения для выравнивания по вертикали start|center|end|space-around|space-between и по горизонтали start|center|end|stretch',
      dataSrc: 'values',
      data: {
        values: [
            {
                label: 'layout-align-center (Выравнивание по центру по вертикали)',
                value: 'layout-align-center'
            },
            {
              label: 'layout-align-center-center (Выравнивание по центру по вертикали и горизонтали)',
              value: 'layout-align-center-center'
            },
            {
              label: 'layout-align-center-end (Выравнивание по центру по вертикали и по правому краю)',
              value: 'layout-align-center-end'
            },
            {
              label: 'layout-align-center-start (Выравнивание по центру по вертикали и по левому краю)',
              value: 'layout-align-center-start'
            },
            {
              label: 'layout-align-end (Выравнивание по нижнему краю)',
              value: 'layout-align-end'
            },
            {
              label: 'layout-align-end-center (Выравнивание по нижнему краю и по центру по гризонтали)',
              value: 'layout-align-end-center'
            },
            {
              label: 'layout-align-end-start (Выравнивание по нижнему левому краю)',
              value: 'layout-align-end-start'
            },
            {
              label: 'layout-align-end-stretch (Выравнивание по нижнему краю по всей ширине)',
              value: 'layout-align-end-stretch'
            },
            {
              label: 'layout-align-space-around (Дочерние элементы располагаются по всей высоте с равным расстоянием от верхней и нижней границы и между друг другом)',
              value: 'layout-align-space-around'
            },
            {
              label: 'layout-align-space-around-center (Дочерние элементы располагаются по всей высоте с равным расстоянием от верхней и нижней границы и между друг другом Выравнивание по центру по горизонтали)',
              value: 'layout-align-space-around-center'
            },
            {
              label: 'layout-align-space-around-end (Дочерние элементы располагаются по всей высоте с равным расстоянием от верхней и нижней границы и между друг другом Выравнивание по правому краю)',
              value: 'layout-align-space-around-end'
            },
            {
              label: 'layout-align-space-around-start (Дочерние элементы располагаются по всей высоте с равным расстоянием от верхней и нижней границы и между друг другом Выравнивание по левому краю)',
              value: 'layout-align-space-around-start'
            },
            {
              label: 'layout-align-space-around-stretch (Дочерние элементы располагаются по всей высоте с равным расстоянием от верхней и нижней границы и между друг другом Выравнивание по всей ширине)',
              value: 'layout-align-space-around-stretch'
            },
            {
              label: 'layout-align-space-between (Дочерние элементы располагаются по всей высоте с нулевым расстоянием от верхней и нижней границы и равным расстоянием между друг другом)',
              value: 'layout-align-space-between'
            },
            {
              label: 'layout-align-space-between-center (Дочерние элементы располагаются по всей высоте с нулевым расстоянием от верхней и нижней границы и равным расстоянием между друг другом Выравнивание по центру по горизонтали)',
              value: 'layout-align-space-between-center'
            },
            {
              label: 'layout-align-space-between-end (Дочерние элементы располагаются по всей высоте с нулевым расстоянием от верхней и нижней границы и равным расстоянием между друг другом Выравнивание по правому краю)',
              value: 'layout-align-space-between-end'
            },
            {
              label: 'layout-align-space-between-start (Дочерние элементы располагаются по всей высоте с нулевым расстоянием от верхней и нижней границы и равным расстоянием между друг другом Выравнивание по левому краю)',
              value: 'layout-align-space-between-start'
            },
            {
              label: 'layout-align-space-between-stretch (Дочерние элементы располагаются по всей высоте с нулевым расстоянием от верхней и нижней границы и равным расстоянием между друг другом Выравнивание по всей ширине)',
              value: 'layout-align-space-between-stretch'
            },
            {
              label: 'layout-align-start (Выравнивание по верхнему краю)',
              value: 'layout-align-start'
            },
            {
              label: 'layout-align-start-center (Выравнивание по верхнему краю и по центру по горизонтали)',
              value: 'layout-align-start-center'
            },
            {
              label: 'layout-align-start-end (Выравнивание по верхнему правому краю)',
              value: 'layout-align-start-end'
            },
            {
              label: 'layout-align-start-start (Выравнивание по верхнему левому краю )',
              value: 'layout-align-start-start'
            },
            {
              label: 'layout-align-start-stretch (Выравнивание по верхнему краю и по всей ширине )',
              value: 'layout-align-start-stretch'
            },
        ]
      },
    },
    {
      weight: 20,
      type: 'select',
      input: true,
      key: 'shrink-section',
      label: 'shrink-section',
      placeholder: '',
      tooltip: 'Свойство shrink section отвечает за сужение контейнеров внутри окна при изменении размеров окна. Значение по умолчанию - shrink',
      dataSrc: 'values',
      data: {
        values: [
            {
                label: 'NOShrink (Не сжимать при недостатке места)',
                value: 'flex-noshrink'
            },
            {
              label: 'Shrink (Сжимать при недостатке места)',
              value: 'flex-shrink'
            },
        ]
      },
    },
    {
      weight: 20,
      type: 'select',
      input: true,
      key: 'grow-section',
      label: 'grow-section',
      placeholder: '',
      tooltip: 'свойство, отвечающее за заполнение пространство полем. Значение по умолчанию - grow',
      dataSrc: 'values',
      data: {
        values: [
            {
                label: 'Auto (Элемент  занимает необходимое пространство)',
                value: 'flex-auto'
            },
            {
              label: 'Grow (Элемент занимает все возможное пространство)',
              value: 'flex-grow'
            },
            {
              label: 'NOGrow (Элемент  сжимается до размеров содержимого)',
              value: 'flex-nogrow'
            },
        ]
      },
    },
    {
      weight: 20,
      type: 'select',
      input: true,
      key: 'layout-wrap',
      label: 'layout-wrap',
      placeholder: '',
      tooltip: 'Позволяет переносить/не переносить элементы, если они занимают более 100% пространства',
      dataSrc: 'values',
      data: {
        values: [
            {
                label: 'layout-nowrap (Дочерние элементы не переносятся, если они используют более 100% пространства)',
                value: 'layout-nowrap'
            },
            {
              label: 'layout-wrap (Дочерние элементы переносятся, если они используют более 100% пространства)',
              value: 'layout-wrap'
            }
        ]
      },
    },
    {
      weight: 20,
      type: 'select',
      input: true,
      key: 'layout-column',
      label: 'layout row/column Столбец',
      placeholder: '',
      tooltip: 'Выравнивание элементов в строку/столбец',
      dataSrc: 'values',
      data: {
        values: [
            {
                label: 'layout-column (Для расположения элементов по вертикали)',
                value: 'layout-column'
            },
            {
              label: 'layout-masonry (Для расположения элементов в стиле masonry (кирпичная кладка))',
              value: 'layout-masonry'
            },
            {
              label: 'layout-row (Для расположения элементов по горизонтали)',
              value: 'layout-row'
            }
        ]
      },
    },
    {
      weight: 20,
      type: 'select',
      input: true,
      key: 'flex-column',
      label: 'flex Столбец',
      placeholder: '',
      tooltip: 'Позволяет столбцу расширяться и сжиматься по мере необходимости. По умолчанию ширина 0%',
      dataSrc: 'values',
      data: {
        values: [
            {
                label: 'flex (Позволяет элементу расширяться и сжиматься по мере необходимости. По умолчанию ширина 0%)',
                value: 'flex'
            }
        ]
      },
    },
    {
      weight: 20,
      type: 'panel',
      title: 'Строка (Порядковый номер)',
      theme: 'default',
      components:[
        {
          type: 'number',
          key: "varcol[0]['value']",
          label: 'xs',
          input: true
        },
        {
          type: 'number',
          key: 'varcol[1]["value"]',
          label: 'sm',
          input: true
        },
        {
          type: 'number',
          key: 'varcol[2]["value"]',
          label: 'md',
          input: true
        },
        {
          type: 'number',
          key: 'varcol[3]["value"]',
          label: 'lg',
          input: true
        },
        {
          type: 'number',
          key: 'varcol[4]["value"]',
          label: 'xl',
          input: true
        },
      ] 
    },
    {
      weight: 20,
      type: 'panel',
      title: 'Ширина',
      theme: 'default',
      components:[
        {
          type: 'number',
          key: 'varwidth-%[0]["value"]',
          label: 'xs',
          input: true
        },
        {
          type: 'number',
          key: 'varwidth-%[1]["value"]',
          label: 'sm',
          input: true
        },
        {
          type: 'number',
          key: 'varwidth-%[2]["value"]',
          label: 'md',
          input: true
        },
        {
          type: 'number',
          key: 'varwidth-%[3]["value"]',
          label: 'lg',
          input: true
        },
        {
          type: 'number',
          key: 'varwidth-%[4]["value"]',
          label: 'xl',
          input: true
        },
      ] 
    },
    {
      weight: 20,
      type: 'panel',
      title: 'Видимость',
      theme: 'default',
      components:[
        {
          type: 'number',
          key: 'visibility-no-grid[0]["value"]',
          label: 'xs',
          input: true
        },
        {
          type: 'number',
          key: 'visibility-no-grid[1]["value"]',
          label: 'sm',
          input: true
        },
        {
          type: 'number',
          key: 'visibility-no-grid[2]["value"]',
          label: 'md',
          input: true
        },
        {
          type: 'number',
          key: 'visibility-no-grid[3]["value"]',
          label: 'lg',
          input: true
        },
        {
          type: 'number',
          key: 'visibility-no-grid[4]["value"]',
          label: 'xl',
          input: true
        },
      ] 
    },
    {
      weight: 20,
      type: 'textfield',
      input: true,
      key: 'varwidth-px',
      label: 'fixed',
      placeholder: ''
    },
  ];
  
  
  export default editFormPositioning;