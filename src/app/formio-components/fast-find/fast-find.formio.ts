import { Injector } from '@angular/core';
import { FastFindComponent } from './fast-find.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'fastfind',
  selector: 'formio-fast-find',
  title: 'fastfind',
  group: 'special',
  icon: 'sticky-note-o',
  editForm: editForm,
  schema: {
    type: 'fastfind',
    varcontrol: 'fastfind',
    idvarcontrol: 49,
    key: 'FASTFIND1',
    varname: 'FASTFIND1',
    varbind: '',
    label: 'Fastfind',
    varlabel: 'Fastfind',
  }
};

export function registerFastFindComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, FastFindComponent, injector);
}