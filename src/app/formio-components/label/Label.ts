import { Components } from 'formiojs';
const BaseComponent = (Components as any).components.base;

export default class LabelElementComponent extends BaseComponent {
    constructor(component, options, data) {
        super(component, options, data);
        this.noField = true;
        this.component = component;
    }
  
    static schema() {
      return BaseComponent.schema({
        type: 'label',
        label:'label',
        varcontrol: 'label',
        idvarcontrol: 12,
        key: 'LABEL1',
        varname: 'LABEL1',
        varbind: 'Label',
        value: '',
        input: false,
        persistent: false
      });
    }
    
    static get builderInfo() {
        return {
            title: 'label',
            group: 'basic',
            icon: 'tag',
            weight: 70,
            schema: LabelElementComponent.schema()
        };
    }

    public render(children) {
      console.log(this.component, this.options, this.data);
      return super.render(this.renderTemplate('labelElement', {
        labelTitle: this.component.varbind,
        border: this.component.varbind.length > 0 ? false : true
      }));
    }

}