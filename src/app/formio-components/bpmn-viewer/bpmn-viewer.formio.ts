import { Injector } from '@angular/core';
import { BpmnViewerComponent } from './bpmn-viewer.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'bpmnViewer',
  selector: 'formio-bpmn-viewer',
  title: 'bpmn-viewer',
  group: 'special',
  icon: 'file-code-o',
  editForm: editForm,
  schema: {
    type: 'bpmn-viewer',
    varcontrol: 'bpmn-viewer',
    idvarcontrol: 61,
    key: 'BPMN-VIEWER1',
    varname: 'BPMN-VIEWER1',
    varbind: '',
    label: 'Bpmn-viewer',
    varlabel: 'Bpmn-viewer',
  }
};

export function registerBpmnViewerComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, BpmnViewerComponent, injector);
}