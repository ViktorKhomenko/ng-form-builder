import { Injector } from '@angular/core';
import { InputDateComponent } from './input-date.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'input[date]',
  selector: 'formio-input-date',
  title: 'input[date]',
  group: 'custom',
  icon: 'calendar-plus-o',
  editForm: editForm,
  schema: {
    type: 'input[date]',
    varcontrol: 'input[date]',
    idvarcontrol: 5,
    key: 'DATE1',
    varname: 'DATE1',
    label: 'Date',
    varlabel: 'Date',
    varbind: '',
    input: false,
    widget: {
        "type": "calendar",
        "altInput": true,
        "allowInput": true,
        "clickOpens": true,
        "enableDate": true,
        "enableTime": true,
        "mode": "single",
        "noCalendar": false,
        "format": "yyyy-MM-dd hh:mm a",
        "dateFormat": "yyyy-MM-ddTHH:mm:ssZ",
        "useLocaleSettings": false,
        "hourIncrement": 1,
        "minuteIncrement": 5,
        "time_24hr": false,
        "saveAs": "date"
    },
  }
};

export function registerInputDateComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, InputDateComponent, injector);
}