import { Injector } from '@angular/core';
import { TemplateComponent } from './template.component';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import  { editForm }  from './edit-form/edit-form.index';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'template',
  selector: 'my-template',
  title: 'template',
  group: 'special',
  icon: 'code',
  editForm: editForm,
  schema: {
    type: 'template',
    varcontrol: 'template',
    idvarcontrol: 18,
    key: 'TEMPLATE1',
    varname: 'TEMPLATE1',
    varbind: '',
    label: 'Template',
    varlabel: 'Template'
  }
};

export function registerTemplateComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, TemplateComponent, injector);
}