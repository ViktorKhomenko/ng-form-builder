import * as _lodash from 'lodash';

let components = _lodash.default.cloneDeep([{
    type: 'tabs',
    key: 'tabs',
    components: [{
      label: 'Общие',
      key: 'general',
      weight: 0,
      IDPROPGROUPTYPE: 1,
      components: []
    }, {
      label: 'Позиционирование',
      key: 'positioning',
      weight: 10,
      IDPROPGROUPTYPE: 2,
      components: []
    },
    {
      label: 'Дополнительные',
      key: 'additional',
      weight: 10,
      components: []
    },
    {
      label: 'Стили',
      key: 'styles',
      weight: 10,
      components: []
    },
    {
      label: 'Валидация',
      key: 'validation',
      weight: 10,
      components: []
    }]
  }]);

export function editForm() {
  return {
    components: components
  }
}