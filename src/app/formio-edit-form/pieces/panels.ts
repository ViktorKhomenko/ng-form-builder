const panels = [
    {name:'Стили', id:3, parent:'', key: 'styles'},
    {name:'Отступы и выравнивание', id:4, key: 'styles'},
    {name:'Стили элементов', id:5, key: 'styles'},
    {name:'Стиль текста', id:6, key: 'styles'},
    {name:'Стили рамки', id:7, key: 'styles'},
    {name:'Стили кнопки', id:8, key: 'additional'},
    {name:'Формат данных', id:9,  key: 'additional'},
    {name:'Tooltip', id:10,  key: 'additional'},
    {name:'Icon', id:11,  key: 'additional'},
    {name:'Popover', id:12},
    {name:'Pagination (только для grid)', id:13,  key: 'additional'},
    {name:'График', id:14,  key: 'additional'},
    {name:'Slider', id:15,  key: 'additional'},
    {name:'Carousel', id:16,  key: 'additional'},
    {name:'Tree', id:17,  key: 'additional'},
    {name:'Switch', id:18,  key: 'additional'},
    {name:'Selection (только для grid)', id:19,  key: 'additional'},
    {name:'Angular Директивы', id:33,  key: 'additional'},
];


export default panels;