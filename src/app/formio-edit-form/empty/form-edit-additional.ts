const editFormAdditional = [
    {
        type: 'panel',
        title: 'Формат данных',
        theme: 'default',
        IDPROPGROUPTYPE:9,
        components: []
    },
    {
        type: 'panel',
        title: 'Tooltip',
        theme: 'default',
        IDPROPGROUPTYPE:10,
        components: []
    },
    {
        type: 'panel',
        title: 'Angular директивы',
        theme: 'default',
        IDPROPGROUPTYPE:33,
        components: []
    },
    {
        type: 'panel',
        title: 'Стиль кнопки',
        theme: 'default',
        IDPROPGROUPTYPE:8,
        components: []
    },
    {
        type: 'panel',
        title: 'Icon',
        theme: 'default',
        IDPROPGROUPTYPE:11,
        components: []
    },
    {
        type: 'panel',
        title: 'Popover',
        theme: 'default',
        IDPROPGROUPTYPE:12,
        components: []
    },
    {
        type: 'panel',
        title: 'Switch',
        theme: 'default',
        IDPROPGROUPTYPE:18,
        components: []
    },
    {
        type: 'panel',
        title: 'Slider',
        theme: 'default',
        IDPROPGROUPTYPE:15,
        components: []
    },
    {
        type: 'panel',
        title: 'График',
        theme: 'default',
        IDPROPGROUPTYPE:14,
        components: []
    },
    {
        type: 'panel',
        title: 'Tree',
        theme: 'default',
        IDPROPGROUPTYPE:17,
        components: []
    },
    {
        type: 'panel',
        title: 'Carousel',
        theme: 'default',
        IDPROPGROUPTYPE:16,
        components: []
    },
    {
        type: 'panel',
        title: 'Selection (только для grid)',
        theme: 'default',
        IDPROPGROUPTYPE:19,
        components: []
    },
    {
        type: 'panel',
        title: 'Pagination (только для grid)',
        theme: 'default',
        IDPROPGROUPTYPE:13,
        components: []
    }
  ];
  
  
  export default editFormAdditional;