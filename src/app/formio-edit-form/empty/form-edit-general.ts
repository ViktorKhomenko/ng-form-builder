const editFormGeneral = [
    {
      weight: 0,
      type: 'textfield',
      input: true,
      key: 'varname',
      label: 'Название поля',
      placeholder: 'Название поля',
      validate: {
        required: true,
      },
    },
    {
      weight: 10,
      type: 'select',
      input: true,
      key: 'idvarcontrol',
      label: 'Тип поля',
      placeholder: '',
      tooltip: '',
      dataSrc: 'values',
      data: {
            values: [
                {
                    label: 'accordion',
                    value: 'accordion'
                },
                {
                  label: 'alert',
                  value: 'alert'
                }, 
                {
                  label: 'art-photo',
                  value: 'artPhoto'
                }, 
                {
                  label: 'autocomplete',
                  value: 'autoComplete'
                }, 
                {
                  label: 'bpmn-modeler',
                  value: 'bpmnModeler'
                },
                {
                  label: 'bpmn-viewer',
                  value: 'bpmnViewer'
                },
                {
                  label: 'button',
                  value: 'button'
                },
                {
                  label: 'button[dropdown]',
                  value: 'buttonDropdown'
                },
                {
                  label: 'button[file]',
                  value: 'buttonFile'
                },
                {
                  label: 'button[group]',
                  value: 'buttonGroup'
                },
                {
                  label: 'chart',
                  value: 'chart'
                },
                {
                  label: 'chart',
                  value: 'chart'
                },
                {
                  label: 'codeMirror',
                  value: 'code-mirror'
                },
                {
                  label: 'component',
                  value: 'component'
                },
                {
                  label: 'fastfind',
                  value: 'fastfind'
                },
                {
                  label: 'fieldset',
                  value: 'fieldset'
                },
                {
                  label: 'g-maps',
                  value: 'gMaps'
                },
                {
                  label: 'grid',
                  value: 'datagrid'
                },
                {
                  label: 'image',
                  value: 'image'
                },
                {
                  label: 'input[checkbox]',
                  value: 'checkbox'
                },
                {
                  label: 'input[date]',
                  value: 'day'
                },
                {
                  label: 'input[datetime]',
                  value: 'inputDateTime'
                },
                {
                  label: 'input[email]',
                  value: 'email'
                },
                {
                  label: 'input[hidden]',
                  value: 'email'
                },
                {
                  label: 'input[password]',
                  value: 'password'
                },
                {
                  label: 'input[password]',
                  value: 'password'
                },
                {
                  label: 'input[radio]',
                  value: 'radio'
                },
                {
                  label: 'input[text]',
                  value: 'texfield'
                },
                {
                  label: 'label',
                  value: 'label'
                },
                {
                  label: 'list-h-container',
                  value: 'list-h-container'
                },
                {
                  label: 'list-v-container',
                  value: 'list-v-container'
                },
                {
                  label: 'md-progress-linear',
                  value: 'mdProgressLinear'
                },
                {
                  label: 'md-slider',
                  value: 'mdSlider'
                },
                {
                  label: 'qrReader',
                  value: 'qr-reader'
                },
                {
                  label: 'qrReader',
                  value: 'qr-reader'
                },
                {
                  label: 'select',
                  value: 'select'
                },
                {
                  label: 'span',
                  value: 'span'
                },
                {
                  label: 'switch',
                  value: 'switch'
                },
                {
                  label: 'tab',
                  value: 'tab'
                },
                {
                  label: 'template',
                  value: 'template'
                },
                {
                  label: 'textarea',
                  value: 'textarea'
                },
                {
                  label: 'treeview',
                  value: 'treeview'
                },
                {
                  label: 'ui-carousel',
                  value: 'uiCarousel'
                },
                {
                  label: 'video',
                  value: 'video'
                }
            ]
        },
      },
    {
      weight: 20,
      type: 'textfield',
      input: true,
      key: 'varbind',
      label: 'Значение',
      placeholder: '',
    },
    {
      weight: 20,
      type: 'textfield',
      input: true,
      key: 'varlabel',
      label: 'Заголовок',
      placeholder: '',
    },
    {
      weight: 20,
      type: 'select',
      input: true,
      key: 'idartdatatype',
      label: 'Тип данных',
      placeholder: '',
      dataSrc: 'values',
      data: {
        values: [
          {
            label: 'string',
            value: 98
          },
          {
            label: 'boolean',
            value: 99
          }, 
          {
            label: 'decimal',
            value: 100
          }, 
          {
            label: 'dateTime',
            value: 101
          }, 
          {
            label: 'time',
            value: 102
          },
          {
            label: 'date',
            value: 103
          }, 
          {
            label: 'integer',
            value: 104
          }, 
        ]
      },
    }
  ];
  
  export default editFormGeneral;