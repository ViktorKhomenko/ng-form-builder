const editFormStyles = [
    {
        type: 'panel',
        title: 'Стили',
        theme: 'default',
        IDPROPGROUPTYPE:3,
        components: []
    },
    {
        type: 'panel',
        title: 'Отступы и выравнивание',
        theme: 'default',
        IDPROPGROUPTYPE:4,
        components: []
    },
    {
        type: 'panel',
        title: 'Стили элементов',
        theme: 'default',
        IDPROPGROUPTYPE:5,
        components: []
    },
    {
        type: 'panel',
        title: 'Стили рамки',
        theme: 'default',
        IDPROPGROUPTYPE:7,
        components: []
    },
    {
        type: 'panel',
        title: 'Стиль текста',
        theme: 'default',
        IDPROPGROUPTYPE:6,
        components: []
    }
  ];
  
  
  export default editFormStyles;