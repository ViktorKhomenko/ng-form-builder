
let builderSidebar = `<div class="formcomponents-cont">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#{{ctx.id}}-tab">Компоненты</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#properties-tab">Свойства</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active container" id="{{ctx.id}}-tab">
                                <div id="builder-sidebar-{{ctx.id}}" class="accordion builder-sidebar" ref="sidebar">
                                    {% ctx.groups.forEach(function(group) { %}
                                        {{ group }}
                                    {% }) %}
                                </div>
                            </div>
                            <div class="tab-pane container" id="properties-tab">
                                <section id="editFormEl" class="edit-form"></section>
                                <div class = "edit-form-loader none" id="edit-form-loader">
                                    <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                                </div>  
                            </div>
                        </div>
                      </div>`;
export default builderSidebar;